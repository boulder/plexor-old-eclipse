package com.wherethisgo.plexor;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.example.games.basegameutils.BaseGameActivity;

public class MainActivity extends BaseGameActivity
{
	public static final String TAG = "DrawingActivity";

	private AlertDialog mAlertDialog;

	// For our intents
	public final static int RC_LOOK_AT_MATCHES = 10001;
	public final static int RC_CREATE_GAME = 10002;
	public static final int RC_OPEN_GAME = 10003;

	// For activities with arguments
	public final static String EXTRA_MATCH_DATA = "com.wherethisgo.plexor.match_data";

	// How long to show toasts.
	private final static int TOAST_DELAY = 2000;

	// This is the current match data after being unpersisted.
	// Do not retain references to match data once you have
	// taken an action on the match, such as takeTurn()
	// public SkeletonTurn mTurnData;

	// private TextView mDisplay;
	// private AtomicInteger msgId = new AtomicInteger();
	// private SharedPreferences prefs;
	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getGameHelper().setMaxAutoSignInAttempts(0);

		context = getApplicationContext();

		// Setup signin button
		findViewById(R.id.button_sign_out).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				signOut();
				findViewById(R.id.button_sign_out).setVisibility(View.GONE);
				findViewById(R.id.sign_out_shadow).setVisibility(View.GONE);
				findViewById(R.id.button_sign_in).setVisibility(View.VISIBLE);
				findViewById(R.id.sign_in_shadow).setVisibility(View.VISIBLE);
				findViewById(R.id.button_create_game).setVisibility(View.GONE);
				findViewById(R.id.create_game_shadow).setVisibility(View.GONE);
			}
		});

		findViewById(R.id.button_sign_in).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// start the asynchronous sign in flow
				beginUserInitiatedSignIn();
				findViewById(R.id.button_sign_out).setVisibility(View.VISIBLE);
				findViewById(R.id.sign_out_shadow).setVisibility(View.VISIBLE);
				findViewById(R.id.button_sign_in).setVisibility(View.GONE);
				findViewById(R.id.sign_in_shadow).setVisibility(View.GONE);
				findViewById(R.id.button_create_game).setVisibility(View.VISIBLE);
				findViewById(R.id.create_game_shadow).setVisibility(View.VISIBLE);
			}
		});
	}

	public void openPlayLocally(View view)
	{
		Intent intent = new Intent(this, PlayLocally.class);
		startActivity(intent);
	}

	// Open the create-game UI. You will get back an onActivityResult
	// and figure out what to do.
	public void onCreateAGameClicked(View view)
	{
		// TODO create a popup side-slider form (possibly) that can be filled in
		// with match settings and then confirmed to create a match

		Intent intent = new Intent(this, CreateGameDialogActivity.class);
		startActivityForResult(intent, RC_CREATE_GAME);
	}

	public void onOpenAGameClicked(View view)
	{
		// TODO consult the function calls below
		// Intent intent = Games.TurnBasedMultiplayer.getInboxIntent(getApiClient());
		// startActivityForResult(intent, RC_LOOK_AT_MATCHES);
		//Intent intent = new Intent(this, ViewGamesDialog.class);
		//startActivityForResult(intent, RC_OPEN_GAME);
	}

	// This function is what gets called when you return from either the Play Games built-in inbox, or else the create game built-in interface.
	@Override
	public void onActivityResult(int request, int response, Intent data)
	{
		// It's VERY IMPORTANT for you to remember to call your superclass. BaseGameActivity will not work otherwise.
		super.onActivityResult(request, response, data);

		if (request == RC_LOOK_AT_MATCHES)
		{
			// Returning from the 'Select Match' dialog

			if (response != Activity.RESULT_OK)
			{
				// user canceled
				return;
			}

			TurnBasedMatch match = data.getParcelableExtra(Multiplayer.EXTRA_TURN_BASED_MATCH);

			if (match != null)
			{
				// updateMatch(match);
			}

			Log.d(TAG, "Match = " + match);
		}
		else if (request == RC_CREATE_GAME)
		{
			/*
			 * -Get game timelimit 
			 * -Get ranked or unranked 
			 * -Create a bitmask from passed data
			 * -TODO Get invited player name, currently only gets the player's ID
			 */

			if (response != Activity.RESULT_OK)
			{
				// User canceled
				return;
			}

			/*TODO add handling for the bitmask in Match.class*/
			long bitmask = 0;
			// get the invitee list
			final ArrayList<String> invitees = data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

			// Values 2-9 are used for these types of matches in the bit mask
			final int moveTimeLimit = data.getIntExtra(CreateGameDialogActivity.EXTRA_MOVE_TIMELIMIT, 7);
			// Values 0-1 are used for these types of matches in the bit mask
			final boolean rankedBool = data.getBooleanExtra(CreateGameDialogActivity.EXTRA_RANKED_BOOLEAN, false);

			if (rankedBool)
				bitmask += 1;

			// Do "+1" to remove the possibility of bitmask being 1. 1 designates a ranked match.
			if (moveTimeLimit != 0)
				bitmask += (moveTimeLimit + 1);

			// Get automatch criteria
			Bundle autoMatchCriteria = null;

			int minAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
			int maxAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);

			if (minAutoMatchPlayers > 0)
			{
				autoMatchCriteria = RoomConfig.createAutoMatchCriteria(minAutoMatchPlayers, maxAutoMatchPlayers, bitmask);
			}
			else
			{
				autoMatchCriteria = null;
			}

			TurnBasedMatchConfig tbmc = TurnBasedMatchConfig.builder().addInvitedPlayers(invitees).setAutoMatchCriteria(autoMatchCriteria).build();

			// Creates a match on google play
			Games.TurnBasedMultiplayer.createMatch(getApiClient(), tbmc).setResultCallback(new ResultCallback<TurnBasedMultiplayer.InitiateMatchResult>()
			{
				@Override
				public void onResult(TurnBasedMultiplayer.InitiateMatchResult result)
				{
					// Calls processResult which opens the match activity
					processResult(result);

				}
			});
		}

		//		else if (request == RC_OPEN_GAME)
		//		{
		//			//TODO take a look at Games.TurnBasedMultiplayer.loadMatch()
		//			// Also look at how Games.TurnBasedMultiplayer.getInboxIntent(getApiClient()); works and using a custom view to select matches
		//
		//			//			Intent intent = Games.TurnBasedMultiplayer.getInboxIntent(getApiClient());
		//			//			startActivityForResult(intent, RC_LOOK_AT_MATCHES);
		//
		//			TurnBasedMatch match = data.getParcelableExtra(Multiplayer.EXTRA_TURN_BASED_MATCH);
		//			String matchId = match.getMatchId();
		//
		//			Games.TurnBasedMultiplayer.loadMatch(getApiClient(), matchId).setResultCallback(new ResultCallback<TurnBasedMultiplayer.LoadMatchResult>()
		//			{
		//				@Override
		//				public void onResult(TurnBasedMultiplayer.LoadMatchResult result)
		//				{
		//					// Calls processResult which opens the match activity
		//					processResult(result);
		//
		//				}
		//			});
		//		}
	}

	private void processResult(TurnBasedMultiplayer.InitiateMatchResult result)
	{
		TurnBasedMatch match = result.getMatch();

		if (!checkStatusCode(match, result.getStatus().getStatusCode()))
		{
			return;
		}

		openMatchView(match);

	}

	private void processResult(TurnBasedMultiplayer.LoadMatchResult result)
	{
		TurnBasedMatch match = result.getMatch();

		if (!checkStatusCode(match, result.getStatus().getStatusCode()))
		{
			return;
		}

		openMatchView(match);

	}

	public void openMatchView(TurnBasedMatch match)
	{
		Intent intent = new Intent(this, Match.class);
		// Pass match data to the activity
		intent.putExtra(EXTRA_MATCH_DATA, match);
		startActivity(intent);
	}

	// Generic warning/info dialog
	protected void showWarning(String title, String message)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle(title).setMessage(message);

		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int id)
			{
				// if this button is clicked, close
				// current activity
			}
		});

		// create alert dialog
		mAlertDialog = alertDialogBuilder.create();

		// Show it
		mAlertDialog.show();
	}

	protected void showErrorMessage(TurnBasedMatch match, int statusCode, int stringId)
	{
		showWarning("Warning", getResources().getString(stringId));
	}

	// Returns false if something went wrong, probably. This should handle
	// more cases, and probably report more accurate results.
	protected boolean checkStatusCode(TurnBasedMatch match, int statusCode)
	{
		switch (statusCode)
		{
		case GamesStatusCodes.STATUS_OK:
			return true;
		case GamesStatusCodes.STATUS_NETWORK_ERROR_OPERATION_DEFERRED:
			// This is OK; the action is stored by Google Play Services and will
			// be dealt with later.
			Toast.makeText(this, "Stored action for later.  (Please remove this toast before release.)", TOAST_DELAY).show();
			// NOTE: This toast is for informative reasons only; please remove
			// it from your final application.
			return true;
		case GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER:
			showErrorMessage(match, statusCode, R.string.status_multiplayer_error_not_trusted_tester);
			break;
		case GamesStatusCodes.STATUS_MATCH_ERROR_ALREADY_REMATCHED:
			showErrorMessage(match, statusCode, R.string.match_error_already_rematched);
			break;
		case GamesStatusCodes.STATUS_NETWORK_ERROR_OPERATION_FAILED:
			showErrorMessage(match, statusCode, R.string.network_error_operation_failed);
			break;
		case GamesStatusCodes.STATUS_CLIENT_RECONNECT_REQUIRED:
			showErrorMessage(match, statusCode, R.string.client_reconnect_required);
			break;
		case GamesStatusCodes.STATUS_INTERNAL_ERROR:
			showErrorMessage(match, statusCode, R.string.internal_error);
			break;
		case GamesStatusCodes.STATUS_MATCH_ERROR_INACTIVE_MATCH:
			showErrorMessage(match, statusCode, R.string.match_error_inactive_match);
			break;
		case GamesStatusCodes.STATUS_MATCH_ERROR_LOCALLY_MODIFIED:
			showErrorMessage(match, statusCode, R.string.match_error_locally_modified);
			break;
		default:
			showErrorMessage(match, statusCode, R.string.unexpected_status);
			Log.d(TAG, "Did not have warning or string to deal with: " + statusCode);
		}

		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	public void onSignInFailed()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onSignInSucceeded()
	{
		// TODO Auto-generated method stub

	}

}
